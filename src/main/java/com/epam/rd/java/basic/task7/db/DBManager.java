package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static DBManager instance;
    private static Connection connection;
    private static final String DATABASE_URL;
    private static final Properties properties = new Properties();

    static {
        try {
            properties.load(new FileReader("app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        DATABASE_URL = (String) properties.get("connection.url");
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private DBManager() throws SQLException {
        connection = DriverManager.getConnection(DATABASE_URL);

    }

    public List<User> findAllUsers() {
        List<User> allUsers = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement
                    .executeQuery("SELECT * FROM users");

            while (resultSet.next()) {
                User user = new User(resultSet.getString("login"));
                user.setId(resultSet.getInt("id"));
                allUsers.add(user);
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allUsers;
    }

    public boolean insertUser(User user) {
        if (user == null) return false;
        try {
            PreparedStatement statement = connection
                    .prepareStatement("INSERT INTO users(login) VALUES(?)");

            statement.setString(1, user.getLogin());
            statement.execute();
            statement.close();

        } catch (SQLException e) {
            e.printStackTrace();

        }
        return true;
    }

    public boolean deleteUsers(User... users) {
        if (users.length == 0) return false;
        StringBuilder stringBuilder = new StringBuilder();

        for (User user : users) {
            stringBuilder
                    .append("'")
                    .append(user.getLogin())
                    .append("',");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        try {
            PreparedStatement statement = connection
                    .prepareStatement("DELETE FROM users WHERE login IN(" + stringBuilder + ")");
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;

    }

    public User getUser(String login) {
        User user = new User(login);
        try {
            PreparedStatement statement = connection
                    .prepareStatement("SELECT * FROM users  WHERE login=?");

            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user.setId(resultSet.getInt("id"));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //       System.out.println(user);
        return user;
    }

    public Team getTeam(String name) {
        Team team = new Team(name);
        try {
            PreparedStatement statement = connection
                    .prepareStatement("SELECT * FROM teams  WHERE name=?");

            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                team.setId(resultSet.getInt("id"));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(team);
        return team;
    }

    public Team getTeamNameById(String id) {
        String teamName = "";
        try {
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT * FROM teams  WHERE id=" + id);
            if (resultSet.next()) {
                teamName = resultSet.getString("name");
                System.out.println(teamName);
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(teamName);
        return new Team(teamName);
    }

    public List<Team> findAllTeams() {
        List<Team> allTeams = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
            while (resultSet.next()) {
                Team team = new Team(resultSet.getString("name"));
                team.setId(resultSet.getInt("id"));
                allTeams.add(team);

            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return allTeams;


    }

    public boolean insertTeam(Team team) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("INSERT INTO teams(name) VALUES(?)");
            statement.setString(1, team.getName());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams)  throws DBException {
        if (user == null || teams == null) return false;
        user.setId(getUser(user.getLogin()).getId());
        try {
            PreparedStatement statement = connection
                    .prepareStatement("INSERT INTO users_teams(user_id,team_id) VALUES (?,?)");
            connection.setAutoCommit(false);
            for (Team team : teams) {
                team.setId(getTeam(team.getName()).getId());
                System.out.println(user.getId() + team.getId());
                statement.setInt(1, user.getId());
                statement.setInt(2, team.getId());
                statement.executeUpdate();

            }
            connection.commit();
            connection.setAutoCommit(true);
            statement.close();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException("transaction has been failed", new SQLException());
        }
        return true;
    }

    public List<Team> getUserTeams(User user) {
        List<Team> userTeams = new ArrayList<>();
        try {
            PreparedStatement statement = connection
                    .prepareStatement("SELECT team_id FROM users_teams WHERE user_id=?");
            statement.setInt(1, user.getId());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                userTeams.add(getTeamNameById(resultSet.getString(1)));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userTeams;
    }

    public boolean deleteTeam(Team team) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("DELETE FROM teams WHERE name=?");
            statement.setString(1, team.getName());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateTeam(Team team) {
        try {
            PreparedStatement statement = connection
                    .prepareStatement("UPDATE teams SET name=? WHERE id=?");
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

}
